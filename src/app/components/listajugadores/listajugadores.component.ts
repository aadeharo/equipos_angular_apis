import { Component, OnInit } from '@angular/core';
import { FifaService } from '../servicios/fifa.service'
import { ActivatedRoute } from "@angular/router";
import { Router } from '@angular/router'

@Component({
  selector: 'app-listajugadores',
  templateUrl: './listajugadores.component.html',
  styleUrls: ['./listajugadores.component.css']
})
export class ListajugadoresComponent implements OnInit {

  lista = {
    idJugador: null,
    idClub: null
  }

  mensajeError : any
  jugadores: any
  mostrar : boolean = false

  constructor(private fifaService:FifaService ,private activatedRouted: ActivatedRoute, private router:Router) {

    this.activatedRouted.params.subscribe(params => {
      console.log("Parametro recibidio es: "+params['id'])
      
      this.fifaService.getListaJugadoresAPI(params['id']).subscribe(data => {
        console.log(data)
        this.jugadores = data.results
        this.mensajeError = data.mensaje
        this.mostrar = true
      },
         error => {
           console.log("fallo el call de la API");
           console.log(this.mensajeError)
         });
      
    }) 

   }

  ngOnInit() {
  }

  vamoDisponible(){
    console.log("Entro bien a vamos disponible");
    
    this.router.navigate( [ '/disponibles'] )
  }

}
