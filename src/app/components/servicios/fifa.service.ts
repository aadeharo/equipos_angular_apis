import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class FifaService {

  afaId: number
  clubid: number
  jugadores: any
  clubes: any    // array de clubes

  constructor(private http: Http) {
    console.log("Servicio listo");
  }

  saveClubID(id: number) {
    this.clubid = id;
    console.log('CLUB ID:', this.clubid);
  }

  saveAfaID(id: number) {
    this.afaId = id;
    console.log('AFA ID :', this.clubid);
  }

  getAfaID() {
    return this.afaId
  }

  getClubID() {
    return this.clubid
  }

  // Trae lista de jugadores de un equipo, id del equipo o afa entra por parametro
  getListaJugadoresAPI(id: number)  //id del club
  {
    console.log("llama a getListaJugadoresAPI");

    let header = new Headers({ 'Content-Type': 'application/json' }); //header del postman
    //let usersURL = "http://192.168.101.252:8080/clubfutbol/club/"+id;    // IP de pc con la api
    let jugURL = "http://localhost:8080/clubfutbol/club/" + id;

    return this.http.get(jugURL, { headers: header })
      .map(res => {
        console.log(res.json());
        console.log("entro positivo REST getListaJugadoresAPI")
        this.jugadores = res.json().results;
        return res.json();
      }, err => console.log("error: " + err.json()));
  }

  getClubesAPI() {
    console.log("llama a getClubesAPI");

    //   cambiar la url

    let header = new Headers({ 'Content-Type': 'application/json' }); //header del postman
    //let usersURL = "http://192.168.101.252:8080/clubfutbol/clubes/";    // IP de pc con la api

    let usersURL = "http://localhost:8080/clubfutbol/clubes"

    return this.http.get(usersURL, { headers: header })
      .map(res => {
        console.log(res.json());
        console.log("entro positivo REST getClubesAPI")
        this.clubes = res.json().results;

        for (const club of this.clubes) {
          if (club.name == 'AFA') {
            this.saveAfaID(club.id)
            console.log("Guardo AFA ID");
            //Guarda el ID del club
          }}
        
        return res.json();
      }, err => console.log("error: " + err.json()));
  }

  setClubesAPI(idc: number, idj: number)  //  Seteo del jugador en club o con afa
  {
    console.log("llama a setClubesAPI");

    //   cambiar la url

    let header = new Headers({ 'Content-Type': 'application/json' }); //header del postman
    // let usersURL = "http://192.168.101.252:8080/clubfutbol/alta"+idc+idj;    // IP de pc con la api
    let usersURL = "http://localhost:8080/clubfutbol/alta" + idc + idj;

    return this.http.get(usersURL, { headers: header })
      .map(res => {
        console.log(res.json());
        console.log("entro positivo REST setClubesAPI")
        //this.jugadores = res.json().results;
        return res.json();
      }, err => console.log("error: " + err.json()));
  }

  // postJugadoresAPI(jugador : any){
  //   {
  //     console.log("llama a la API de altaJugador");
      
  //        let header = new Headers({'Content-Type':'application/json'}); //header del postman
  //        let jugURL = "http://localhost:8080/clubfutbol/alta";    // IP de pc con la api
  //        let body = jugador;
      
  //         return this.http.post(jugURL, body, {headers: header})
  //            .map(res =>{
  //              console.log(res.json());
  //              console.log("entro positivo REST postHeroesAPI")
  //              //this.heroes = res.json().results;     //   guarda en el array de heroes y no el mensaje de error
  //              return res.json();
  //            }, err => console.log("error: "+err.json()));
  //    }

  // }
}


      /* jugadoresjason = {
        name
        age
        img
      } */

      /* 192.168.101.252 */


