import { Component, OnInit } from '@angular/core';
import { FifaService } from '../servicios/fifa.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-disponibles',
  templateUrl: './disponibles.component.html',
  styleUrls: ['./disponibles.component.css']
})
export class DisponiblesComponent implements OnInit {

jugadores:any
mensajeError:any
mostrar:any

  constructor(private fifaService:FifaService ,private activatedRouted: ActivatedRoute, private router:Router) { 
   
    this.fifaService.getListaJugadoresAPI(this.fifaService.getAfaID()).subscribe(data => {
      console.log(data)
      this.jugadores = data.results
      this.mensajeError = data.mensaje + " estamos en getListaJugadoresAPI disponibles component"
      this.mostrar = true
    },
       error => {
         console.log("fallo el call de la API");
         console.log(this.mensajeError)
       });
  }

  ficharJugadores(id:number){
    this.fifaService.setClubesAPI(this.fifaService.getClubID(),id)
    console.log("Se ha fichado a un guachin",);
    this.router.navigate( [ '/listajugadores',this.fifaService.getClubID()] )

    
  }

  ngOnInit() {
  }

}
