import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { NgForm } from '@angular/forms';
import { FifaService } from '../servicios/fifa.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  mostrar : boolean = false
  clubid : number
  clubes : any
  //errorMen : 'No coinciden los datos ingresados con la base de datos del sistema'

  clubUsuario = {
    name : '',
    password : '',
    logo : ''
  }

  constructor(private fifaService:FifaService, private router:Router) { }

  ngOnInit() 
  {

    //        TRAE LISTA DE CLUBES
    this.fifaService.getClubesAPI()   
      .subscribe(data => {    //ejecuta la API
      console.log(data)
      this.clubes = data.results;     //    es el nombre del array de clubes
      this.clubid = data.id;
      this.fifaService.saveClubID(this.clubid)  //Guarda el ID del club
      this.mostrar = true
      },
      error => {
        console.log("fallo el call de la API getClubesAPI");
        console.log(error)
     });

    console.log(this.clubes);                           

  }


  datosIngresados(forma:NgForm){
    for (const club of this.clubes) {
      if (this.clubUsuario.name == club.name)
    {
      if (this.clubUsuario.password == club.password)
      {
        if (this.clubUsuario.name == 'AFA')
        {
          this.setClubID(club.id)
          this.router.navigate( [ '/altajugador'] )
        }
        else
        {
          this.setClubID(club.id)
          this.router.navigate( [ '/listajugadores',club.id] )
        }
      } //password
    } //user 
    }
  }  // datosIngresados

  setClubID(id:number){
    console.log('id del club '+id);
    this.fifaService.saveClubID(id) //seteo id del club que hace login
   } //metodo setID

  /* comprobarPassword(forma:NgForm){
    console.log('Llamada a ');
    

    this.clubUsuario.name = this.fifaService.get()
    this.comentario.idUser = this.blogsService.getUserID()

    this.blogsService.comentarPost(this.comentario)
      .subscribe(data => {    //ejecuta la API
      console.log(data)
      },
      error => {
        console.log("fallo el call de la API, desde commentPost post.component.ts");
        console.log(error)
     }); */

}
